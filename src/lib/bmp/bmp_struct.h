#ifndef LLP_LAB_5_6__BMP_STRUCT_H
#define LLP_LAB_5_6__BMP_STRUCT_H

#include <stdint.h>

/**
 * Структура BMP File Header. Хранит информацию о файле
 */
typedef struct __attribute__((packed)){
    /**
     * Отметка для отличия формата от других (сигнатура формата)
     */
    uint16_t    bfType;

    /**
     * Размер файла в байтах
     */
    uint32_t    bfFileSize;

    /**
     * Зарезервирован и должны содержать ноль.
     */
    uint32_t    bfReserved;

    /**
     * Положение пиксельных данных относительно начала данной структуры (в байтах).
     */
    uint32_t    bfOffBits;

} BitmapFileHeader;

/**
 * Содержит ширину, высоту и битность растра, а также формат пикселей, информацию о цветовой таблице и разрешении.
 */
typedef struct __attribute__((packed)) {
    /**
     * Размер данной структуры в байтах, указывающий также на версию структуры
     * <ul>
     *  <li>12 байт - BitmapCoreHeader</li>
     *  <li>40 байт - BitmapInfoHeader</li>
     *  <li>108 байт - BitmapV4Header</li>
     *  <li>124 байт - BitmapV5Header</li>
     * </ul>
     */
    uint32_t    biSize;

    /**
     * Ширина (bcWidth) растра в пикселях. Указывается целым числом без знака. Значение 0 не документировано.
     */
    int32_t     biWidth;

    /**
     * Высота (bcHeight) растра в пикселях. Указываются целым числом без знака. Значение 0 не документировано.
     */
    int32_t     biHeight;

    /**
     * В BMP допустимо только значение 1. Это поле используется в значках и курсорах Windows.
     */
    uint16_t    biPlanes;

    /**
     * Количество бит на пиксель
     */
    uint16_t    biBitCount;

    /**
     * Указывает на способ хранения пикселей
     */
    uint32_t    biCompression;

    /**
     * Размер пиксельных данных в байтах. Может быть обнулено если хранение осуществляется двумерным массивом.
     */
    uint32_t    biSizeImage;

    /**
     * Количество пикселей на метр по горизонтали
     */
    int32_t    biXPelsPerMeter;

    /**
     * Количество пикселей на метр по вертикали
     */
    int32_t    biYPelsPerMeter;

    /**
     * Размер таблицы цветов в ячейках.
     */
    uint32_t    biClrUsed;

    /**
     * Количество ячеек от начала таблицы цветов до последней используемой (включая её саму).
     */
    uint32_t    biClrImportant;

} BitmapInfoHeader;

/**
 * присутствует только в том случае, если в Info.BitsPerPixel менее 8
 * следует упорядочить цветов по важности
 */
typedef struct __attribute__((packed)){
    uint8_t     red;
    uint8_t     green;
    uint8_t     blue;
    uint8_t     reserved;
} ColorTable;

typedef struct __attribute__((packed)) {
    unsigned pix:1;
} PixelData1Bit;

typedef struct __attribute__((packed)) {
    unsigned pix:4;
} PixelData4Bit;

typedef struct __attribute__((packed)) {
    uint8_t pix;
} PixelData8Bit;

typedef struct __attribute__((packed)) {
    unsigned x:1;
    unsigned r:5;
    unsigned g:5;
    unsigned b:5;
} PixelData16Bit;

typedef struct __attribute__((packed)){
    uint8_t     blue;
    uint8_t     green;
    uint8_t     red;
} PixelData24Bit;

typedef struct {
    PixelData1Bit*      pixelData1Bit;
    PixelData4Bit*      pixelData4Bit;
    PixelData8Bit*      pixelData8Bit;
    PixelData16Bit*     pixelData16Bit;
    PixelData24Bit*     pixelData24Bit;
} Pixels;

typedef struct {
    char*               filename;
    BitmapFileHeader    fileHeader;
    BitmapInfoHeader    infoHeader;
    ColorTable*         colorTable;
    Pixels              pixels;
} BMP;

#endif //LLP_LAB_5_6__BMP_STRUCT_H
