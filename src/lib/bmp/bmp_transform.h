#ifndef LLP_LAB_5_6__BMP_TRANSFORM_H
#define LLP_LAB_5_6__BMP_TRANSFORM_H

#include <stdio.h>
#include <memory.h>

#include "bmp_struct.h"
#include "bmp_io.h"
#include "../malloc/custom_mem.h"

#define malloc _malloc

void printInformationBMP(BMP *bmp);

BMP* rotate(BMP *bmpFile, int angelOfDegrees);
PixelData24Bit* getPixel(BMP *bmp, int x, int y);

#endif //LLP_LAB_5_6__BMP_TRANSFORM_H
