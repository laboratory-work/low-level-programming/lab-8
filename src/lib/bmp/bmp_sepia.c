#include <stdlib.h>
#include <math.h>
#include "bmp_sepia.h"

static const float int_to_float_convert[256] = {
        0.000000f, 1.000000f, 2.000000f, 3.000000f, 4.000000f, 5.000000f, 6.000000f, 7.000000f, 8.000000f, 9.000000f,
        10.000000f, 11.000000f, 12.000000f, 13.000000f, 14.000000f, 15.000000f, 16.000000f, 17.000000f, 18.000000f,
        19.000000f, 20.000000f, 21.000000f, 22.000000f, 23.000000f, 24.000000f, 25.000000f, 26.000000f, 27.000000f,
        28.000000f, 29.000000f, 30.000000f, 31.000000f, 32.000000f, 33.000000f, 34.000000f, 35.000000f, 36.000000f,
        37.000000f, 38.000000f, 39.000000f, 40.000000f, 41.000000f, 42.000000f, 43.000000f, 44.000000f, 45.000000f,
        46.000000f, 47.000000f, 48.000000f, 49.000000f, 50.000000f, 51.000000f, 52.000000f, 53.000000f, 54.000000f,
        55.000000f, 56.000000f, 57.000000f, 58.000000f, 59.000000f, 60.000000f, 61.000000f, 62.000000f, 63.000000f,
        64.000000f, 65.000000f, 66.000000f, 67.000000f, 68.000000f, 69.000000f, 70.000000f, 71.000000f, 72.000000f,
        73.000000f, 74.000000f, 75.000000f, 76.000000f, 77.000000f, 78.000000f, 79.000000f, 80.000000f, 81.000000f,
        82.000000f, 83.000000f, 84.000000f, 85.000000f, 86.000000f, 87.000000f, 88.000000f, 89.000000f, 90.000000f,
        91.000000f, 92.000000f, 93.000000f, 94.000000f, 95.000000f, 96.000000f, 97.000000f, 98.000000f, 99.000000f,
        100.000000f, 101.000000f, 102.000000f, 103.000000f, 104.000000f, 105.000000f, 106.000000f, 107.000000f,
        108.000000f, 109.000000f, 110.000000f, 111.000000f, 112.000000f, 113.000000f, 114.000000f, 115.000000f,
        116.000000f, 117.000000f, 118.000000f, 119.000000f, 120.000000f, 121.000000f, 122.000000f, 123.000000f,
        124.000000f, 125.000000f, 126.000000f, 127.000000f, 128.000000f, 129.000000f, 130.000000f, 131.000000f,
        132.000000f, 133.000000f, 134.000000f, 135.000000f, 136.000000f, 137.000000f, 138.000000f, 139.000000f,
        140.000000f, 141.000000f, 142.000000f, 143.000000f, 144.000000f, 145.000000f, 146.000000f, 147.000000f,
        148.000000f, 149.000000f, 150.000000f, 151.000000f, 152.000000f, 153.000000f, 154.000000f, 155.000000f,
        156.000000f, 157.000000f, 158.000000f, 159.000000f, 160.000000f, 161.000000f, 162.000000f, 163.000000f,
        164.000000f, 165.000000f, 166.000000f, 167.000000f, 168.000000f, 169.000000f, 170.000000f, 171.000000f,
        172.000000f, 173.000000f, 174.000000f, 175.000000f, 176.000000f, 177.000000f, 178.000000f, 179.000000f,
        180.000000f, 181.000000f, 182.000000f, 183.000000f, 184.000000f, 185.000000f, 186.000000f, 187.000000f,
        188.000000f, 189.000000f, 190.000000f, 191.000000f, 192.000000f, 193.000000f, 194.000000f, 195.000000f,
        196.000000f, 197.000000f, 198.000000f, 199.000000f, 200.000000f, 201.000000f, 202.000000f, 203.000000f,
        204.000000f, 205.000000f, 206.000000f, 207.000000f, 208.000000f, 209.000000f, 210.000000f, 211.000000f,
        212.000000f, 213.000000f, 214.000000f, 215.000000f, 216.000000f, 217.000000f, 218.000000f, 219.000000f,
        220.000000f, 221.000000f, 222.000000f, 223.000000f, 224.000000f, 225.000000f, 226.000000f, 227.000000f,
        228.000000f, 229.000000f, 230.000000f, 231.000000f, 232.000000f, 233.000000f, 234.000000f, 235.000000f,
        236.000000f, 237.000000f, 238.000000f, 239.000000f, 240.000000f, 241.000000f, 242.000000f, 243.000000f,
        244.000000f, 245.000000f, 246.000000f, 247.000000f, 248.000000f, 249.000000f, 250.000000f, 251.000000f,
        252.000000f, 253.000000f, 254.000000f, 255.000000f
};

void nope() {  }

void sepia_c_inplace(BMP *img) {
    PixelData24Bit *pixels = img->pixels.pixelData24Bit;
    long width = img->infoHeader.biWidth;
    long height = img->infoHeader.biHeight;

    for(long i = 0; i < height * width; i++) {
        sepia_one(pixels + i);
    }
}

static uint8_t sat(float x) {
    uint8_t out = x < 256.0f ? (int) x : 255;
    return out;
}

void sepia_one(PixelData24Bit *const pixel) {
    int red = pixel->red;
    int green = pixel->green;
    int blue = pixel->blue;

    float red_float = round(red * .393f + green * .769f + blue * .189f);
    float green_float = round(red * .349f + green * .686f + blue * .168f);
    float blue_float = round(red * .272f + green * .534f + blue * .131f);

    nope();

    pixel->red = sat(red_float);
    pixel->green = sat(green_float);
    pixel->blue = sat(blue_float);
}

void sepia_sse_inplace(BMP* img) {
    PixelData24Bit *pixels = img->pixels.pixelData24Bit;
    long width = img->infoHeader.biWidth;
    long height = img->infoHeader.biHeight;

    float b_colors[4];
    float g_colors[4];
    float r_colors[4];
    uint8_t res[12];

    long max = width * height;
    int block = max / 4;

    for(int i = 0; i < block; i++) {
        PixelData24Bit *pix = pixels + i * 4;
        for (int j = 0; j < 4; j++) {
            PixelData24Bit *p = pix + j;
            r_colors[j] = int_to_float_convert[p->red];
            g_colors[j] = int_to_float_convert[p->green];
            b_colors[j] = int_to_float_convert[p->blue];
        }
        sepia_sse(r_colors, g_colors, b_colors, res);
        for(int j = 0; j < 4; j++) {
            PixelData24Bit *p = pix + j;
            p->red =   res[3*j    ];
            p->green = res[3*j + 1];
            p->blue =  res[3*j + 2];
        }
        nope();
    }
}