#ifndef LLP_LAB_5_6__BMP_H
#define LLP_LAB_5_6__BMP_H

#include <stdlib.h>
#include "bmp_struct.h"
#include "bmp_io.h"
#include "bmp_transform.h"
#include "../malloc/custom_mem.h"

/**
 * Функция открывает bmp файл.
 * Читает только заголовки, осуществляет их проверку
 *
 * @param filename - путь к bmp файлу
 * @return ссылка на struct BMP
 */
BMP* open(char* filename);

/**
 *  Читает пиксели непосредственно
 */
void read(BMP* bmp_file);

void write(BMP* bmp);

void write_newFile(BMP* bmp_file, char* filename);

/**
 * Функция закрывает bmp файл.
 *
 * @param bmp - struct BMP*
 * @return код возврата. <ul>
 *      <il>0 - успешно</il>
 *      <il>1 - не успешно</il>
 *  </ul>
 */
int close(BMP* bmp);

#endif //LLP_LAB_5_6__BMP_H
