#ifndef LLP_LAB_5_6__BMP_IO_H
#define LLP_LAB_5_6__BMP_IO_H

#include "bmp_struct.h"
#include <stdlib.h>
#include <stdio.h>
#include <sysexits.h>
#include "../malloc/custom_mem.h"

#define malloc _malloc

BMP* readHeader(char *filename);

/**
 * Читает файл и записывает в струткуру BMP.
 *
 * @return ссылку на структуру BMP.
 */
void readPixels(BMP* bmp);

/**
 * Записывает струткуру BMP в файл по пути newFilename.
 *
 * @return Код возврата. 0 - успешно записано
 */
int writeBMP(BMP *bmp) ;

/**
 * Создает пустую стркутуры BMP файла
 * @return ссылку на структуру BMP.
 */
BMP* createEmptyBMP();

BMP* copyEmptyBMP(BMP *bmpFile);

BMP* copyFullBMP(BMP *bmpFile);

#endif //LLP_LAB_5_6__BMP_IO_H
