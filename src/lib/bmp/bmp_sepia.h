#ifndef LLP_LAB_8_BMP_SEPIA_H
#define LLP_LAB_8_BMP_SEPIA_H
#include "bmp.h"

void sepia_c_inplace(BMP* img);
void sepia_sse_inplace(BMP* img);

static uint8_t sat(float x);
void sepia_one(PixelData24Bit* const pixel);
void sepia_sse(float *r_colors, float *g_colors, float *b_colors, uint8_t *res);

#endif //LLP_LAB_8_BMP_SEPIA_H
