%define r_array rdi
%define g_array rsi
%define b_array rdx
%define result rcx

%macro return_v 0
    cvtps2dq xmm0, xmm0
    pminsd xmm0, [maximum]

    pextrb [result    ], xmm0, 0        ; R1
    pextrb [result + 1], xmm0, 4        ; G1
    pextrb [result + 2], xmm0, 8        ; B1
    pextrb [result + 3], xmm0, 12       ; R2
%endmacro

; R1 G1 B1 R2 | G2 B2 R3 G3 | B3 R4 G4 B4


%macro shift_array_values 0
    add r_array, 4
    add g_array, 4
    add b_array, 4
    add result, 4
%endmacro

align 16
maximum: dd 255,255,255,255

align 16
c1_1: dd 0.393, 0.349, 0.272, 0.393 ; R
align 16
c2_1: dd 0.769, 0.686, 0.534, 0.769 ; G
align 16
c3_1: dd 0.189, 0.168, 0.131, 0.189 ; B

align 16
c1_2: dd 0.349, 0.272, 0.393, 0.349
align 16
c2_2: dd 0.686, 0.543, 0.769, 0.686
align 16
c3_2: dd 0.168, 0.131, 0.189, 0.168

align 16
c1_3: dd 0.272, 0.393, 0.349, 0.272
align 16
c2_3: dd 0.534, 0.769, 0.686, 0.534
align 16
c3_3: dd 0.131, 0.189, 0.168, 0.131

global sepia_sse

; r1 r1 r1 r2 | r2 r2 r3 r3 | r3 r4 r4 r4
; g1 g1 g1 g2 | g2 g2 g3 g3 | g3 g4 g4 g4
; b1 b1 b1 b2 | b2 b2 b3 b3 | b3 b4 b4 b4

section .text
sepia_sse:
    movq xmm0, [r_array] ; r1 r2 0 0
    shufps xmm0, xmm0, 0b01000000
    movaps xmm3, [c1_1]
    mulps  xmm0, xmm3

    movq xmm1, [g_array] ; g1 g2 0 0
    shufps xmm1, xmm1, 0b01000000
    movaps xmm4, [c2_1]
    mulps  xmm1, xmm4

    movq xmm2, [b_array] ; b1 b2 0 0
    shufps xmm2, xmm2, 0b01000000
    movaps xmm5, [c3_1]
    mulps  xmm2, xmm5

    addps  xmm0, xmm1
    addps  xmm0, xmm2

    return_v

    shift_array_values

    movq xmm0, [r_array] ; r2 r3 0 0
    shufps xmm0, xmm0, 0b01010000
    movaps xmm3, [c1_2]
    mulps  xmm0, xmm3

    movq xmm1, [g_array] ; g2 g3 0 0
    shufps xmm1, xmm1, 0b01010000
    movaps xmm4, [c2_2]
    mulps  xmm1, xmm4

    movq xmm2, [b_array] ; b2 b3 0 0
    shufps xmm2, xmm2, 0b01010000
    movaps xmm5, [c3_2]
    mulps  xmm2, xmm5

    addps  xmm0, xmm1
    addps  xmm0, xmm2

    return_v

    shift_array_values
    movq xmm0, [r_array] ; r3 r4 0 0
    shufps xmm0, xmm0, 0b01010100
    movaps xmm3, [c1_3]
    mulps  xmm0, xmm3

    movq xmm1, [g_array] ; g3 g4 0 0
    shufps xmm1, xmm1, 0b01010100
    movaps xmm4, [c1_3]
    mulps  xmm1, xmm4

    movq xmm2, [b_array] ; b3 b4 0 0
    shufps xmm2, xmm2, 0b01010100
    movaps xmm5, [c3_3]
    mulps  xmm2, xmm5

    addps  xmm0, xmm1
    addps  xmm0, xmm2

    return_v

    ret