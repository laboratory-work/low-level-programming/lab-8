#ifndef __CUSTOM_MEM_H__
#define __CUSTOM_MEM_H__

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <stdlib.h>

#ifndef __USE_MISC
#define __USE_MISC
/* Mapping type (must choose one and only one of these).  */
#define MAP_FILE	 0x0001	/* Mapped from a file or device.  */
#define MAP_ANON	 0x0002	/* Allocated from anonymous virtual memory.  */
#define MAP_TYPE	 0x000f	/* Mask for type field.  */
#define MAP_ANONYMOUS	 MAP_ANON /* Linux name. */
#endif // !__USE_MISC

#ifndef HEAP_START
#define HEAP_START ((void*)0x04040000)
#endif

#ifndef BLOCK_MIN_SIZE
#define BLOCK_MIN_SIZE 4098
#endif // !BLOCK_MIN_SIZE

#define DEBUG_FIRST_BYTES 4

#pragma pack(push, 1)
typedef struct mem_t {
	struct mem_t* next;
	size_t capacity;
	int is_free;
} mem_t;
#pragma pack(pop)

void* heap_init(size_t init_size);
void* _malloc(size_t request);
void _free(void* mem);

void mem_alloc_debug_struct_info(FILE* f, mem_t const* address);
void mem_alloc_debug_heap(FILE* f, mem_t const* ptr);

#endif // !__CUSTOM_MEM_H__
