#include "custom_mem.h"

#define __mmap(addr, size) mmap(addr, round_size_block_size(size), PROT_READ | PROT_WRITE, MAP_ANON | MAP_PRIVATE, -1, 0)

int isHeapInit = 0;
const size_t sizeof_mem_t = sizeof(struct mem_t);

size_t round_size_block_size(size_t n) {
    size_t ost = n % BLOCK_MIN_SIZE;
    return ost ? n + (BLOCK_MIN_SIZE - ost) : n;
}

struct mem_t *find_available_block(struct mem_t *head, size_t request) {
    while (head) {
        if (head->is_free && head->capacity >= request + sizeof_mem_t)
            return head;
        head = head->next;
    }
    return NULL;
}

struct mem_t *get_last_block(struct mem_t *head) {
    while (head->next) {
        head = head->next;
    }
    return head;
}

struct mem_t *allocate_block(struct mem_t *last, size_t request) {
    char *addr = (char *) last + last->capacity + sizeof_mem_t;
    char *alloc = __mmap(addr, request);

    if (alloc == MAP_FAILED) {
        alloc = __mmap(NULL, request);
    }

    if (alloc == MAP_FAILED) {
        fprintf(stderr, "cannot allocate memory");
        return NULL;
    }

    if (addr == alloc) {
        last->capacity += round_size_block_size(request);
        return last;
    } else {
        last->next = (struct mem_t *) alloc;
        last->next->next = NULL;
        last->next->capacity = round_size_block_size(request) - sizeof_mem_t;
        last->next->is_free = 1;
        return last->next;
    }
}

void cut_if_need(struct mem_t *last, size_t request) {
    struct mem_t *free = NULL;
    if (last->capacity >= request + sizeof_mem_t + sizeof_mem_t) {
        free = (struct mem_t *) ((char *) last + request + sizeof_mem_t);
        free->capacity = last->capacity - request - sizeof_mem_t;
        free->next = NULL;
        free->is_free = 1;

        last->next = free;
        last->capacity = request;
        last->is_free = 0;
    }
}

struct mem_t *find_mem_block(struct mem_t *start, struct mem_t *target) {
    while (start->next) {
        if (start == target) { return start; }
        start = start->next;
    }
    return NULL;
}

void merge_blocks() {
    int repeat = 1;
    while (repeat) {
        repeat = 0;
        struct mem_t *head = HEAP_START;
        while (head && head->next) {
            if (head->is_free && head->next->is_free) {
                char *addr = (char *) head + sizeof_mem_t + head->capacity;
                if (addr == (char *) head->next) {
                    head->capacity += head->next->capacity + sizeof_mem_t;
                    head->next = head->next->next;
                    head->is_free = 1;
                    repeat = 1;
                }
            }
            head = head->next;
        }
    }
}

void *heap_init(size_t init_size) {
    void *ptr = __mmap(HEAP_START, init_size);
    if (ptr == MAP_FAILED) {
        fprintf(stderr, "Heap init didn't allocated");
        return NULL;
    }
    struct mem_t *head = ptr;
    head->next = NULL;
    head->capacity = round_size_block_size(init_size) - sizeof_mem_t;
    head->is_free = 1;
    isHeapInit = 1;
    return (char *) head + sizeof_mem_t;
}

void *_malloc(size_t request) {
    if (isHeapInit) {
        struct mem_t *ptr = find_available_block(HEAP_START, request);
        struct mem_t *new_mem = NULL;

        if (ptr) {
            cut_if_need(ptr, request);
            return (char *) ptr + sizeof_mem_t;
        } else {
            ptr = get_last_block(HEAP_START);
            new_mem = allocate_block(ptr, request);
            cut_if_need(new_mem, request);
            return (char *) new_mem + sizeof_mem_t;
        }
    } else {
        fprintf(stderr, "Heap is not initially");
        return NULL;
    }
}

void _free(void *mem) {
    if (isHeapInit) {
        struct mem_t *block = find_mem_block(HEAP_START, (struct mem_t *) ((char *) mem - sizeof_mem_t));
        if (block) {
            block->is_free = 1;
            merge_blocks();
        }
    } else {
        fprintf(stderr, "Heap is not initially");
    }
}