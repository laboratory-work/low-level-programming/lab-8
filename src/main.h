#ifndef LLP_LAB_7_MAIN_H
#define LLP_LAB_7_MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>
#include "lib/malloc/custom_mem.h"
#include "lib/console/console_handler.h"
#include "lib/bmp/bmp.h"
#include "lib/bmp/bmp_sepia.h"

int main(int argc, char** argv);

#endif //LLP_LAB_7_MAIN_H
