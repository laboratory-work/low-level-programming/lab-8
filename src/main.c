#include "main.h"
#include <time.h>

void gen() {
    long width = 10240;
    long height = 10240;

    BMP* bigImage = createEmptyBMP();
    bigImage->filename = "bigimg.bmp";
    bigImage->fileHeader.bfType = 0x4d42;
    bigImage->fileHeader.bfReserved = 0;
    bigImage->fileHeader.bfOffBits = 54;

    bigImage->infoHeader.biWidth = width;
    bigImage->infoHeader.biHeight = height;
    bigImage->infoHeader.biCompression = 0;
    bigImage->infoHeader.biPlanes = 1;
    bigImage->infoHeader.biBitCount = 24;
    bigImage->infoHeader.biSizeImage = width * height * 3;
    bigImage->infoHeader.biXPelsPerMeter = 2834;
    bigImage->infoHeader.biYPelsPerMeter = 2834;
    bigImage->infoHeader.biClrUsed = 0;
    bigImage->infoHeader.biClrImportant = 0;

    PixelData24Bit *pixels = calloc(width * height, sizeof(PixelData24Bit));
    bigImage->pixels.pixelData24Bit = pixels;

    FILE* fp = fopen("/dev/urandom", "r");

    for(size_t i = 0; i < width * height; i++) {
        fread(&((pixels+i)->red), sizeof(uint8_t), 1, fp);
        fread(&((pixels+i)->green), sizeof(uint8_t), 1, fp);
        fread(&((pixels+i)->blue), sizeof(uint8_t), 1, fp);
    }

    write(bigImage);
}

void get_benchmark_sepia(char* test, void(*func)(BMP*), BMP* file) {
    struct timespec tstart={0,0}, tend={0,0};
    clock_gettime(CLOCK_MONOTONIC, &tstart);
    func(file);
    clock_gettime(CLOCK_MONOTONIC, &tend);
    double diff = ((double)tend.tv_sec*1.0e9 + tend.tv_nsec) -
                  ((double)tstart.tv_sec*1.0e9 + tstart.tv_nsec);
    printf("%s took about %.1f nanoseconds\n", test, diff);
}

int main(int argc, char** argv) {
    if(!heap_init(1)) {
        exit(EX_OSERR);
    }

//    attrConsole console = analize(argc, argv);
    BMP* bmpFile = open("transform/bigimg.bmp");
    if(bmpFile == NULL) {
        exit(1);
    }
    read(bmpFile);

//    printInformationBMP(bmpFile);

    BMP* sepia_c = copyFullBMP(bmpFile);
    get_benchmark_sepia("sepia c   inplace", sepia_c_inplace, sepia_c);
//    sepia_c_inplace(sepia_c);

//    write_newFile(sepia_c, "sepia-c.bmp");
    close(sepia_c);

    BMP* sepia_sse = copyFullBMP(bmpFile);
    get_benchmark_sepia("sepia sse inplace", sepia_sse_inplace, sepia_sse);
//    sepia_sse_inplace(sepia_sse);

//    write_newFile(sepia_sse, "sepia-sse.bmp");
    close(sepia_sse);
    
    close(bmpFile);
    return 0;
}